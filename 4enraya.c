#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#define t 9
#define b 12
#define RESET_COLOR "\x1b[0m"
#define ROJO_F "\x1b[41m"
#define AMARILLO_F "\x1b[43m"
 
int jugador = 1;
int jugador2 = 2;
int tablero [t][t];
int contador = 0;
int contador_jugadas = 0;
int jugardenuevo;

void victoria_JugadorSolo (){
	
		for (int i = 1; i<t; i++){ 
			for (int j = 1 ; j<t ; j++){
			
			if (tablero [i][j] == jugador && tablero [i-1][j+1] == jugador && tablero [i-2][j+2] == jugador && tablero [i-3][j+3] == jugador){ //Condicón diagonal ascendente ( / ).

				printf ("\nHA GANADO EL JUGADOR 1 EN DIAGONAL HACIA ARRIBA.");
				contador = 1;
				contador_jugadas++;
				break;
				}
		
			else if (tablero [i][j] == jugador && tablero [i+1][j+1] == jugador && tablero [i+2][j+2] == jugador && tablero [i+3][j+3] == jugador){ //Condición diagonal descendente ( \ ).
			
				printf ("\nHA GANADO EL JUGADOR 1 EN DIAGONAL HACIA ABAJO.");
				contador = 1;
				contador_jugadas++;
				break;
				}
				
			else if (tablero [i][j] == jugador && tablero [i][j+1] == jugador && tablero [i][j+2] == jugador && tablero [i][j+3] == jugador){ //Condición horizontal ( - )
				
				printf ("\nHA GANADO EL JUGADOR 1 EN HORIZONTAL.");			
				contador = 1;
				contador_jugadas++;
				break;
				}
				
			else if (tablero [i][j] == jugador && tablero [i+1][j] == jugador && tablero [i+2][j] == jugador && tablero [i+3][j] == jugador){ //Condición vertical ( | )

				printf ("\nHA GANADO EL JUGADOR 1 EN VERTICAL.");
				contador = 1;
				contador_jugadas++;
				break;
				}
			}
		}
		system("clear");	
}

void victoria_CPU (){
	
	for (int i = 1; i<t; i++){ 
		for (int j = 1 ; j<t ; j++){
		
		if (tablero [i][j] == jugador2 && tablero [i-1][j+1] == jugador2 && tablero [i-2][j+2] == jugador2 && tablero [i-3][j+3] == jugador2){ //Condicón diagonal ascendente ( / ).

			printf ("\nHA GANADO LA CPU EN DIAGONAL HACIA ARRIBA.");
			contador = 1;
			contador_jugadas=2;
			break;
			}
	
		else if (tablero [i][j] == jugador2 && tablero [i+1][j+1] == jugador2 && tablero [i+2][j+2] == jugador2 && tablero [i+3][j+3] == jugador2){ //Condición diagonal descendente ( \ ).
		
			printf ("\nHA GANADO LA CPU EN DIAGONAL HACIA ABAJO.");
			contador = 1;
			contador_jugadas=2;
			break;
			}
			
		else if (tablero [i][j] == jugador2 && tablero [i][j+1] == jugador2 && tablero [i][j+2] == jugador2 && tablero [i][j+3] == jugador2){ //Condición horizontal ( - )
			
			printf ("\nHA GANADO LA CPU EN HORIZONTAL.");			
			contador = 1;
			contador_jugadas=2;
			break;
			}
			
		else if (tablero [i][j] == jugador2 && tablero [i+1][j] == jugador2 && tablero [i+2][j] == jugador2 && tablero [i+3][j] == jugador2){ //Condición vertical ( | )

			printf ("\nHA GANADO LA CPU EN VERTICAL.");
			contador = 1;
			contador_jugadas=2;
			break;
			}
		}
	}
	system("clear");
}
void victoria_jugador2(){
	for (int i = 1; i<t ; i++){ 
		for (int j = 1 ; j<t ; j++){
		
		if (tablero [i][j] == jugador2 && tablero [i-1][j+1] == jugador2 && tablero [i-2][j+2] == jugador2 && tablero [i-3][j+3] == jugador2){ //Condicón diagonal ascendente ( / ).

			printf ("\nHA GANADO EL JUGADOR %d EN DIAGONAL.\n\n", jugador2);
			contador = 1;
			contador_jugadas=2;
			break;
			}
	
		else if (tablero [i][j] == jugador2 && tablero [i+1][j+1] == jugador2 && tablero [i+2][j+2] == jugador2 && tablero [i+3][j+3] == jugador2){ //Condición diagonal descendente ( \ ).
		
			printf ("\nHA GANADO EL JUGADOR %d EN DIAGONAL.\n\n", jugador2);
			contador = 1;
			contador_jugadas=2;
			break;
			}
			
		else if (tablero [i][j] == jugador2 && tablero [i][j+1] == jugador2 && tablero [i][j+2] == jugador2 && tablero [i][j+3] == jugador2){ //Condición horizontal ( - )
			
			printf ("\nHA GANADO EL JUGADOR %d EN HORIZONTAL.\n\n", jugador2);
			contador = 1;
			contador_jugadas=2;
			break;
			}
			
		else if (tablero [i][j] == jugador2 && tablero [i+1][j] == jugador2 && tablero [i+2][j] == jugador2 && tablero [i+3][j] == jugador2){ //Condición vertical ( | )

			printf ("\nHA GANADO EL JUGADOR %d EN VERTICAL.\n\n", jugador2);
			contador = 1;
			contador_jugadas=2;
			break;
			}
		}
	
	}
}
void victoria_jugador1 (){
	
	for (int i = 1; i<t ; i++){ 
		for (int j = 1 ; j<t ; j++){
		
		if (tablero [i][j] == jugador && tablero [i-1][j+1] == jugador && tablero [i-2][j+2] == jugador && tablero [i-3][j+3] == jugador){ //Condicón diagonal ascendente ( / ).

			printf ("\nHA GANADO EL JUGADOR %d EN DIAGONAL.\n\n", jugador);
			contador = 1;
			contador_jugadas=1;
			break;
			}
	
		else if (tablero [i][j] == jugador && tablero [i+1][j+1] == jugador && tablero [i+2][j+2] == jugador && tablero [i+3][j+3] == jugador){ //Condición diagonal descendente ( \ ).
		
			printf ("\nHA GANADO EL JUGADOR %d EN DIAGONAL.\n\n", jugador);
			contador = 1;
			contador_jugadas=1;
			break;
			}
			
		else if (tablero [i][j] == jugador && tablero [i][j+1] == jugador && tablero [i][j+2] == jugador && tablero [i][j+3] == jugador){ //Condición horizontal ( - )
			
			printf ("\nHA GANADO EL JUGADOR %d EN HORIZONTAL.\n\n", jugador);
			contador = 1;
			contador_jugadas=1;
			break;
			}
			
		else if (tablero [i][j] == jugador && tablero [i+1][j] == jugador && tablero [i+2][j] == jugador && tablero [i+3][j] == jugador){ //Condición vertical ( | )

			printf ("\nHA GANADO EL JUGADOR %d EN VERTICAL.\n\n", jugador);
			contador = 1;
			contador_jugadas=1;
			break;
			}
		}
	}
}

void inicializacion (){
	
	for (int i=1; i<t ; i++){
		for (int j=1; j<t; j++){
			
			tablero [i][j]= 0;
		}
	}
}

void impresiontab (){
	
	
	for (int i=1; i<t ; i++){
		for (int j=1; j<t; j++){
			
			if (tablero [i][j] == 0){
				
				printf (" [ ] ");
			}
			else{
				
				printf (" [%d] ", tablero [i][j]);
			}
		}
		printf ("\n");
	}
}

void jugadasolo(){
	
	int jugada;
	
	do{
			printf ("\nIngrese el número de la columna en la cual desea jugar: ");
			scanf ("%d", &jugada);
		
	}while (tablero [1][jugada] != 0);
	
	for (int i=1; i<t ; i++){
		
		if (tablero[i][jugada] == 0){
			
			tablero [i][jugada]=1;
			//sleep (1); //probar que se puede ganar de forma mas rapida sin sleep.
			
			impresiontab();
			
			tablero [i-1][jugada]=0;
			
			impresiontab ();
			
		}
	jugador++;	
	
	}
}

void jugadamulti(){
	int jugada1;
	
	do{	
		
		printf ("\nTURNO DEL JUGADOR 1");
		printf ("\n\nIngrese el número de la columna en la cual desea jugar: ");
		scanf ("%d", &jugada1);
		
	}while (tablero [1][jugada1] != 0 && tablero[1][jugada1] != 9);
	
	printf ("\n ==DIVIERTETE JUGANDO AL 4 EN LÍNEA== \n\n");
	for (int i=1; i<t ; i++){
		
		if (tablero[i][jugada1] == 0){
			
			tablero [i][jugada1] = 1;
			//sleep (1); // probar que se puede ganar de forma mas rapida sin sleep.
			
			impresiontab();
			system ("clear");
			
			tablero [i-1][jugada1] = 0 ;
			
			impresiontab ();
		}
	jugador++;	
	}

	int jugada2;
	
	do{
		printf ("\n ==DIVIERTETE JUGANDO AL 4 EN LÍNEA== \n\n");
		
		printf ("\nTURNO DEL JUGADOR 2");
		printf ("\n\nIngrese el número de la columna en la cual desea jugar: ");
		scanf ("%d", &jugada2);
		
	}while (tablero [1][jugada2] != 0);
	
	for (int i=1; i<t ; i++){
		
		if (tablero[i][jugada2] == 0){
			
			tablero [i][jugada2]= 2;
			//sleep (1); // probar que se puede ganar de forma mas rapida sin sleep.
			
			impresiontab();
			
			tablero [i-1][jugada2]=0;
			
			impresiontab ();
		}
	jugador--;
	}
}

void bot(){
	
	int jugada;
	
	do{
		jugada= (rand () %8 + 1);
		
				
	}while (tablero [1][jugada] != 0);
	
	for (int i=1; i<t ; i++){
		
		if (tablero[i][jugada] == 0){
			
			tablero [i][jugada]=2;
			//sleep (1); // probar que se puede ganar de forma mas rapida sin sleep.
			
			impresiontab();
			
			tablero [i-1][jugada] = 0;
			
			impresiontab ();
			
		}
	jugador--;	
	
	}
}

/*void retry (){
	
	inicializacion ();
	srand (time(NULL));
	
		
	printf ("\nHAS ESCOGIDO JUGAR SOLO V/S CPU.\n");
	printf ("\n");
	sleep (1);
	system ("clear");
		
	printf ("\n ==DIVIERTETE JUGANDO AL 4 EN LÍNEA== \n\n");
		
	impresiontab();
		
	if (jugador==1){
		
		jugadasolo ();
		system ("clear");
	}
	else{
		//sleep (1);
		printf("\nCPU PENSANDO...\n");
		//sleep (1);
		printf ("\n[JUGADA PREPADA]\n");
		//sleep (1);
		bot();
		system("clear");
	}

	victoria_CPU();
	victoria_JugadorSolo();

}
*/

int main(){
	
	int contador=0;
	char opcion;
	
	printf ("\t	=BIENVENIDO A 4 EN FILA=\n");
	printf ("\t		  BY\n");
	printf ("     	    Gerardo Muñoz & Benjamín Rojas\n");
	printf ("   	   Ingeniería Civil en Bioinformática\n");
	printf ("\n");
	
	sleep (1);
	
	printf ("\n:>Proyecto final Soluciones Algorítmicas, Universidad de Talca 2018<:\n");
	
	sleep (2);
	system ("clear");
	
	do{
		
		printf ("\nMODO DE JUEGO: ¿SINGLEPLAYER OR MULTIPLAYER? (PRESS S/ PRESS M): ");
		scanf (" %c", &opcion);
		
	}while (opcion != 'm' && opcion != 's' && opcion != 'S' && opcion != 'M');
	
	inicializacion ();
	srand (time(NULL));
	
	if (opcion == 'S' || opcion == 's'){
		
		printf ("\nHAS ESCOGIDO JUGAR SOLO V/S CPU.\n");
		printf ("\n");
		sleep (1);
		system ("clear");
		
		do{
			printf ("\n ==DIVIERTETE JUGANDO AL 4 EN LÍNEA== \n\n");
			
			impresiontab();
			
			if (jugador==1){
				
				jugadasolo ();
				system ("clear");
			}
			else{
				//sleep (1);
				printf("\nCPU PENSANDO...\n");
				//sleep (1);
				printf ("\n[JUGADA PREPADA]\n");
				//sleep (1);
				bot();
				system("clear");
			}
		
			victoria_CPU();
			victoria_JugadorSolo();
			
			if (contador_jugadas == 1){
				printf ("\n\n¡¡HASTA LUEGO!!\n");
				printf ("¡GRACIAS POR JUGAR!\n");
				break;				
			}
			
			else if (contador_jugadas == 2){
				printf ("\n\n¡¡HASTA LUEGO!!\n");
				printf ("¡GRACIAS POR JUGAR!\n");
				break;
			
			}
			
						
		}while(contador != 1);
	}
	
	else if (opcion == 'M' || opcion == 'm'){
		
		printf ("\nHAS ESCOGIDO JUGAR CONTRA ALGUIEN.\n");
		printf ("\n");
		sleep (1);
		system ("clear");
		
		do{
			printf ("\n ==DIVIERTETE JUGANDO AL 4 EN LÍNEA== \n\n");
			
			impresiontab();
			
			jugadamulti();
			system("clear");
			
			victoria_jugador1();
						
			if (contador_jugadas == 1){
				printf ("\n\n¡¡HASTA LUEGO!!\n");
				printf ("¡GRACIAS POR JUGAR!\n");
				break;
				break;				
			}
			
			victoria_jugador2();
			
			if(contador_jugadas == 2){
				printf ("\n\n¡¡HASTA LUEGO!!\n");
				printf ("¡GRACIAS POR JUGAR!\n");
				break;
			
			}
			
		}while (contador != 1);
	}	
		
	return 0;
}

