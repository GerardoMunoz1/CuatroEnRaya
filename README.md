        +=========CUATRO EN RAYA=========+
		|==Ing. Civil en Bioinformática==|
		|=Gerardo Muñoz & Benjamín Rojas=|
		|	       :: 2018 ::   		 |


::DESCRIPCIÓN DEL JUEGO::

Se trata de un juego de tablero, con el mismo situado en posición vertical, 
formando una mampara que separa a los dos jugadores que compiten. Cada jugador 
dispone de grandes fichas planas con forma de moneda, de un color diferente para
cada uno de ellos.

::OBJETIVO DEL JUEGO::

La finalidad del juego es que cada jugador logre conectar 4 fichas en forma de 
una hilera, tanto horizontal, vertical, como también diagonal. Quien logre 
conectar 4 fichas en cualquiera de sus formas mencionadas anteriormente, vence 
al rival, obteniendo la victoria. Si el tablero se llena y no existe ningún 
ganador, el juego se da por empatado.

::REGLAS DEL JUEGO::

-Al iniciar el juego, se solicitará un modo de juego. Si el usuario desea 
jugar solo, este jugará v/s CPU; de lo contrario, si desea jugar en multijugador,
se debe decidir fuera del código quién será el jugador 1 y jugador 2.

-Se deben ingresar las fichas entre los números 1 y 8.

-Si la columna se encuentra llena de fichas de ambos jugadores, se debe repetir 
el turno hasta que el usuario ingrese la ficha en una columna que no se encuentre
llena.

-Si uno de los dos jugadores gana, el juego se cierra anunciando quien ha salido 
victorioso. Para volver a jugar se debe reiniciar el programa.

::POSIBLES ERRORES EN EL CÓDIGO::

-Si el usuario ingresa un número menor que uno (0) o un número mayor que 8, el 
código recibirá el número ingresado, sin embargo no jugará en ninguna columna y 
le tocará al otro jugador (CPU o JUGADOR2).

-Si el usuario ingresa una letra en el momento en que le preguntan en que columna 
jugará, generará un error y la terminal finalizará la ejecución del programa.

-En el modo de juego "multiplayer", si gana el jugador uno, el código terminará 
el ciclo del "for", por lo que jugará el jugador 2 también. Sin embargo, la jugada 
de este, no influirá en la victoria del jugador 1.
